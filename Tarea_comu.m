%%
%Senal Cuadrada
frec_sq=1/(2*(93e-6));%frecuencia de senal
fs = 30*frec_sq;%frecuencia para plot
fm = 1/(2*fs); %Periodo de muestreo para analizador de espectros de simulink
offset_sq=0.5;%offset de se�al (sino esta centrada en y =0)
amp_sq=0.5;
t = 0:1/fs:1-1/fs;
sq_wav= offset_sq + amp_sq*square(2*pi*frec_sq*t);%senal cuadrada
plot(t,sq_wav)
ylim([-0.5 1.5])
xlim([0 0.001])
title('Se�al cuadrada (Tp = 93us)');
xlabel('Tiempo(s)');
%%
%Senal Cuadrada en Frecuencia
fftSignal = fft(sq_wav)/length(sq_wav);%transformada fourier(division para tener amp que se supone)
fftSignal = fftshift(fftSignal); %esto tira fourier centrada en f = 0 
f = fs/2*linspace(-1,1,fs);%puntos de plot
plot(f, abs(fftSignal));
title('Espectro en frecuencia: Pulsos Cuadrados');
xlabel('Frecuencia (Hz)');
ylabel('Magnitud');

%%
%Filtro para se�al cuadrada (Es un RC de primer orden)
s = tf('s');
R = 1060;
C = 4e-9;
filtro = 1/(R*C*(s+(1/(R*C))));
bode(filtro)
%%
%Tono cosenoidal
frec_tono = 184000;%cambia este
fs_tono = 20*frec_tono;
t_tono = 0:1/fs_tono:1-1/fs_tono;
tono = cos(2*pi*frec_tono*t_tono);
plot(t_tono,tono);
xlim([0 0.00001])
ylim([-1.1 1.1])
%constantes de intermodulacion para simulink
a1 = 0.74;
a2 = 0 ;
a3 = 0.74;
a4 = 0.47;
a5 = 4.52;
frec = 2*pi*frec_tono;
Tm = 1/(20*frec_tono); %Periodo de muestreo para analizador de espectros de simulink
%%
%Senal Tono cosenoidal en Frecuencia
fftSignal = fft(tono)/length(tono);
fftSignal = fftshift(fftSignal);
f = fs_tono/2*linspace(-1,1,fs_tono);
plot(f, abs(fftSignal));
title('Espectro en frecuencia: Tono Cosenoidal');
xlabel('Frequency (Hz)');
ylabel('Magnitud');

%%
%Suma de Tonos
frec_tn1 = 39000;%frecuencia de tono 1
frec_tn2 = 960000;%frecuencia de tono 2
fs_tn1 = 20*frec_tn1;
fs_tn2 = 20*frec_tn2;
t_tn1 = 0:1/fs_tn1:1-1/fs_tn1;
t_tn2 = 0:1/fs_tn2:1-1/fs_tn2;
tn1 = cos(2*pi*frec_tn1*t_tn1);
tn2 = cos(2*pi*frec_tn2*t_tn2);
plot(t_tn1,tn1,t_tn2,tn2);
xlim([0 0.00005])
ylim([-1.1 1.1])
%constantes de intermodulacion para simulink
a1 = 0.74;
a2 = 0 ;
a3 = 0.74;
a4 = 0.47;
a5 = 4.52;
frec1 = 2*pi* frec_tn1;
frec2 = 2*pi* frec_tn2;
Tm = 1/(5*frec_tn2); %Periodo de muestreo para analizador de espectros de simulink
%%
%Senal suma de tonos cosenoidal en Frecuencia
fftSignal1 = fft(tn1)/length(tn1);
fftSignal2 = fft(tn2)/length(tn2);
fftSignal1 = fftshift(fftSignal1);
fftSignal2 = fftshift(fftSignal2);
f1 = fs_tn1/2*linspace(-1,1,fs_tn1);
f2 = fs_tn2/2*linspace(-1,1,fs_tn2);
plot(f1, abs(fftSignal1),f2, abs(fftSignal2));
title('Espectro en frecuencia: Suma de Tonos');
xlabel('Frequency (Hz)');
ylabel('Magnitud');

%%
%CODIGO EJEMPLO DE COMO HACER ESPECTRO
fs = 4000;
f = 400; %Hz
%Define signal
t = 0:1/fs:1-1/fs;
signal = cos(2*pi*f*t);
%Plot to illustrate that it is a sine wave
plot(t, signal);
title('Time-Domain signal');
%%
%CODIGO EJEMPLO DE COMO HACER ESPECTRO
%Take fourier transform
fftSignal = fft(signal);
%apply fftshift to put it in the form we are used to (see documentation)
fftSignal = fftshift(fftSignal);
%Next, calculate the frequency axis, which is defined by the sampling rate
f = fs/2*linspace(-1,1,fs);
%Since the signal is complex, we need to plot the magnitude to get it to
%look right, so we use abs (absolute value)
figure;
plot(f, abs(fftSignal));
title('magnitude FFT of sine');
xlabel('Frequency (Hz)');
ylabel('magnitude');
