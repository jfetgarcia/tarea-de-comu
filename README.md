# Tarea de Comu 

Progra de matlab para tarea de comunicaciones electricas 1 

## Uso

Para realizar las graficas de los espectros se utilizaron los 
archivos de simulink que se encuentran en el repositorio. Se utilizo
matlab 2018 por lo que esta es la versión minima para poder accederlos

En el archivo **Tarea comu.m** se generan los parametros que utilizan
el simulink para generar las graficas. El codigo esta separado en
secciones para hacer todo mas modular.

Hay secciones que no se terminaron utilizando ya que el simulink 
resulto ser suficiente. Estas son las secciones con las transf. de
Fourier.

## Senal Cuadrada

Esta requiere ejecutar la sección **Senal cuadrada** y la sección con
el titulo de **filtro**. Tener en mente que en mi caso personal, decidi
utilizar un filtro pasivo RC. Solo cree la función de transferencia y 
la ingrese a simulink. Si desea otro filtro, tendria que cambiar esta
funcion de transferencia.

**Datos a cambiar**:
- Frecuencia de la senal cuadrada (solo requiere ingresar su Tp)
- Valores R y C necesarios (en caso de que no se cambie el filtro)

## Productos de intermodulacion

 Requiere ejecutar la seccion titulada **Suma de tonos**

**Datos a cambiar**: 
- Valores de frecuencia para los tonos
- Valores de los parametros a

**NOTA**: en el parametro Tm se debe utilizar la frecuencia de mayor 
magnitud. En mi caso era la frec2 pero podria no ser igual para todos

## THD

 Requiere ejecutar la seccion titulada **Tono**

 **Datos a cambiar**: 
- Valor de frecuencia para el tono
- Valores de los parametros a
